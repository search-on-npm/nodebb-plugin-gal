"use strict";

var controllers = require('./lib/controllers'),
	sockets = require('./lib/sockets'),
	async = require.main.require('async'),
	db = require.main.require('./src/database'),
	winston = require.main.require('winston'),

	SocketPlugins = require.main.require('./src/socket.io/plugins'),

	isLatinGAL = /#[\w\d\-_.]+$/,
	removePunctuationSuffix = function(string) {
		return string.replace(/[!?.,]*$/, '');
	},

	plugin = {
		templates: {}
	};

SocketPlugins.connectgal = sockets;

plugin.init = function (params, callback) {
	params.router.get('/admin/plugins/connect-gal', params.middleware.admin.buildHeader, controllers.renderAdmin);
	params.router.get('/api/admin/plugins/connect-gal', controllers.renderAdmin);

	callback();
};

plugin.addAdminNavigation = function (header, callback) {

	header.plugins.push({
		route: '/plugins/connect-gal',
		icon: 'fa-file-image-o',
		name: 'GAL'
	});

	callback(null, header);
};

plugin.parsePost = function(data, callback) {
	// winston.verbose("[connect-gal] parsePost Start");
	if (!data || !data.postData || !data.postData.content) {
		return callback(null, data);
	}

	plugin.parseRaw(data.postData.content, function(err, content) {
		if (err) {
			return callback(err);
		}

		data.postData.content = content;
		callback(null, data);
	});
};

plugin.parseRaw = function(content, callback) {
	var start = 0;
	var stop = -1;

	var set = 'connect-gal';

	async.waterfall([
		function (next) {
			db.getSortedSetRevRange(set, start, stop, next);
		},
		function (gids, next) {
			// Gids: Gal IDS
			if (!Array.isArray(gids) || !gids.length) {
				return next(null, []);
			}

			var keys = gids.map(function(gid) {
				return set+':gid:' + gid;
			});
			// Ritorna la lista di tutti gli elementi del GAL
			db.getObjects(keys, function(err, items) {
				if (err) {
					return next(err);
				}
				next(null, items);
			});
		}
	], function(err, items) {
		if(err) {
			return callback(err);
		}

		async.each(items, function(item, next) {
			if(!item || !item.name) {
				return next();
			}

			var match = removePunctuationSuffix(item.name);

			var regex = isLatinGAL.test(match)
				? new RegExp('(?:^|>|\\s)('+match+')(?=\\s|<|[,|.|?|!]|$)', 'i')
				: new RegExp('(?:^|>|\\s)('+match+')(?=\\s|<|[,|.|?|!]|$)', 'i');

			content = content.replace(regex, function(match) {
				// Again, cleaning up lookaround leftover bits
				var atIndex = match.toLowerCase().indexOf(item.name.toLowerCase()),
					plain = match.slice(0, atIndex),
					match = match.slice(atIndex),
					str = '<a class="plugin-gal-a" href="' + item.url + '" data-gid="' + item.gid + '" target="_blank" title="'+ item.text +'"><i class="fa fa-external-link"></i> <span>' + match + '</span></a>';

				return plain + str;
			});
			next();

		}, function(err) {
			callback(err, content);
		});
	});
};

module.exports = plugin;

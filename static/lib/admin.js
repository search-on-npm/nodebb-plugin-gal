'use strict';
/* globals $, app, socket, Sortable */

define('admin/plugins/connect-gal', function() {

    var ACP = {};

    ACP.init = function() {

        $('#group-search').keyup(function() {
            var search_text = $('#group-search').val();
            var reg = new RegExp(search_text, 'ig');
            $('#gal-list tr').not('.gal-notsearch').each(function() {
                if(reg.test($(this).find('td').not('.gal-notsearch').text())) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });

        document.getElementById("item_new").addEventListener("click", function(){
            loadModal();
        })

        document.querySelectorAll(".item_update").forEach(item => {
            item.addEventListener("click", function() {
                socket.emit("plugins.connectgal.get", { gid: item.dataset.gid }, function(err, values){
                    loadModal(values)
                })
            })
        })

        document.querySelectorAll(".item_delete").forEach(item => {
            item.addEventListener("click", function() {
                socket.emit("plugins.connectgal.delete", { gid: item.dataset.gid }, function(err, result){
                    if (err) {
                        app.alertError(err.message)
                        return false
                    }

                    app.alert({
                        type: 'success',
                        alert_id: 'connect-gal-save',
                        title: result.title,
                        message: result.message,
                        clickfn: function() {
                            socket.emit('admin.restart');
                        }
                    })
                    ajaxify.refresh();
                    return true;
                })
            })
        })
    };

    function loadModal(values) {        
        app.parseAndTranslate('admin/modals/connect-gal', {}, function(tpl) {
            var modal = bootbox.dialog(
                {
                    message: tpl,
                    title: 'GAL',
                    buttons: {
                        success: {
                            label: "Save",
                            className: "btn-primary save",
                            callback: function() {
                                return saveFields();
                            }
                        }
                    }
                }
            );
            modal.on('shown.bs.modal', function() {
                componentHandler.upgradeAllRegistered();
                if (typeof(values) != undefined) {
                    document.getElementById("connect_gal_name").value = values["name"]
                    document.getElementById("connect_gal_url").value = values["url"]
                    document.getElementById("connect_gal_text").value = values["text"]
                    document.getElementById("connect_gal_gid").value = values["gid"]
                }
            });
        });
    }

    function saveFields() {
        data = {
            "name": document.getElementById("connect_gal_name").value,
            "url": document.getElementById("connect_gal_url").value,
            "text": document.getElementById("connect_gal_text").value,
            "gid": document.getElementById("connect_gal_gid").value
        }
 
        document.querySelectorAll("#connect_gal_frm .form-group").forEach(element => {
            element.classList.remove("has-error")
        })

        var err = {
            flag: false,
            errors: [],
            fields: []
        };
        if(!data.name) {
            err.flag = true;
            err.fields.push("name");
            err.errors.push("Name is required");
        }
        if(!data.url) {
            err.flag = true;
            err.fields.push("url");
            err.errors.push("URL is required");
        } else if(!isURL(data.url)) {
            err.flag = true;
            err.fields.push("url");
            err.errors.push("URL is not a valid URL");
        }
        if(!data.text) {
            err.flag = true;
            err.fields.push("text");
            err.errors.push("Text is required");
        }

        if (err.flag) {
            err.fields.forEach(field => {
                document.querySelector("[data-group='" + field + "']").classList.add("has-error");
            })
            app.alertError("<br>"+err.errors.join(("<br>")));
            return false;
        }
        socket.emit('plugins.connectgal.save', {data:data}, function(err, result) {
            if (err) {
                app.alertError(err.message);
                return false;
            }

            app.alert({
                type: 'success',
                alert_id: 'connect-gal-save',
                title: result.title,
                message: result.message,
                clickfn: function() {
                    socket.emit('admin.restart');
                }
            });
            ajaxify.refresh();
            return true;
        });
    }

    function isURL(url, options) {
        var default_url_options = {
            protocols: [ 'http', 'https', 'ftp' ]
          , require_tld: true
          , require_protocol: false
          , require_valid_protocol: true
          , allow_underscores: false
          , allow_trailing_dot: false
          , allow_protocol_relative_urls: false
        };
        if (!url || url.length >= 2083 || /\s/.test(url)) {
            return false;
        }
        if (url.indexOf('mailto:') === 0) {
            return false;
        }
        //options = merge(options, default_url_options);
        options = default_url_options;
        var protocol, auth, host, hostname, port,
            port_str, split;
        split = url.split('://');
        if (split.length > 1) {
            protocol = split.shift();
            if (options.require_valid_protocol && options.protocols.indexOf(protocol) === -1) {
                return false;
            }
        } else if (options.require_protocol) {
            return false;
        }  else if (options.allow_protocol_relative_urls && url.substr(0, 2) === '//') {
            split[0] = url.substr(2);
        }
        url = split.join('://');
        split = url.split('#');
        url = split.shift();

        split = url.split('?');
        url = split.shift();

        split = url.split('/');
        url = split.shift();
        split = url.split('@');
        if (split.length > 1) {
            auth = split.shift();
            if (auth.indexOf(':') >= 0 && auth.split(':').length > 2) {
                return false;
            }
        }
        hostname = split.join('@');
        split = hostname.split(':');
        host = split.shift();
        if (split.length) {
            port_str = split.join(':');
            port = parseInt(port_str, 10);
            if (!/^[0-9]+$/.test(port_str) || port <= 0 || port > 65535) {
                return false;
            }
        }
        if (!isIP(host) && !isFQDN(host, options) &&
                host !== 'localhost') {
            return false;
        }
        if (options.host_whitelist &&
                options.host_whitelist.indexOf(host) === -1) {
            return false;
        }
        if (options.host_blacklist &&
                options.host_blacklist.indexOf(host) !== -1) {
            return false;
        }
        return true;
    }

    function isIP(str, version) {
        var ipv4Maybe = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/
      , ipv6Block = /^[0-9A-F]{1,4}$/i;
        version = version ? version + '' : '';
        if (!version) {
            return isIP(str, 4) || isIP(str, 6);
        } else if (version === '4') {
            if (!ipv4Maybe.test(str)) {
                return false;
            }
            var parts = str.split('.').sort(function (a, b) {
                return a - b;
            });
            return parts[3] <= 255;
        } else if (version === '6') {
            var blocks = str.split(':');
            var foundOmissionBlock = false; // marker to indicate ::

            // At least some OS accept the last 32 bits of an IPv6 address
            // (i.e. 2 of the blocks) in IPv4 notation, and RFC 3493 says
            // that '::ffff:a.b.c.d' is valid for IPv4-mapped IPv6 addresses,
            // and '::a.b.c.d' is deprecated, but also valid.
            var foundIPv4TransitionBlock = isIP(blocks[blocks.length - 1], 4);
            var expectedNumberOfBlocks = foundIPv4TransitionBlock ? 7 : 8;

            if (blocks.length > expectedNumberOfBlocks)
                return false;

            // initial or final ::
            if (str === '::') {
                return true;
            } else if (str.substr(0, 2) === '::') {
                blocks.shift();
                blocks.shift();
                foundOmissionBlock = true;
            } else if (str.substr(str.length - 2) === '::') {
                blocks.pop();
                blocks.pop();
                foundOmissionBlock = true;
            }

            for (var i = 0; i < blocks.length; ++i) {
                // test for a :: which can not be at the string start/end
                // since those cases have been handled above
                if (blocks[i] === '' && i > 0 && i < blocks.length -1) {
                    if (foundOmissionBlock)
                        return false; // multiple :: in address
                    foundOmissionBlock = true;
                } else if (foundIPv4TransitionBlock && i == blocks.length - 1) {
                    // it has been checked before that the last
                    // block is a valid IPv4 address
                } else if (!ipv6Block.test(blocks[i])) {
                    return false;
                }
            }

            if (foundOmissionBlock) {
                return blocks.length >= 1;
            } else {
                return blocks.length === expectedNumberOfBlocks;
            }
        }
        return false;
    }

    function isFQDN(str, options) {
        var default_fqdn_options = {
            require_tld: true
          , allow_underscores: false
          , allow_trailing_dot: false
        };
        //options = merge(options, default_fqdn_options);
        options = default_fqdn_options;

        /* Remove the optional trailing dot before checking validity */
        if (options.allow_trailing_dot && str[str.length - 1] === '.') {
            str = str.substring(0, str.length - 1);
        }
        var parts = str.split('.');
        if (options.require_tld) {
            var tld = parts.pop();
            if (!parts.length || !/^([a-z\u00a1-\uffff]{2,}|xn[a-z0-9-]{2,})$/i.test(tld)) {
                return false;
            }
        }
        for (var part, i = 0; i < parts.length; i++) {
            part = parts[i];
            if (options.allow_underscores) {
                if (part.indexOf('__') >= 0) {
                    return false;
                }
                part = part.replace(/_/g, '');
            }
            if (!/^[a-z\u00a1-\uffff0-9-]+$/i.test(part)) {
                return false;
            }
            if (/[\uff01-\uff5e]/.test(part)) {
                // disallow full-width chars
                return false;
            }
            if (part[0] === '-' || part[part.length - 1] === '-') {
                return false;
            }
            if (part.indexOf('---') >= 0 && part.slice(0, 4) !== 'xn--') {
                return false;
            }
        }
        return true;
    }

    return ACP;
});
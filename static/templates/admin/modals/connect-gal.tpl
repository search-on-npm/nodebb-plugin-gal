<div>
	<form role="form" id="connect_gal_frm">
		<div class="form-group" data-group="name">
			<label for="connect_gal_name">Name</label>
			<input type="text" id="connect_gal_name" name="name" data-field="name" title="Name" class="form-control" placeholder="" value="{connect_gal.title}"><br />
		</div>
		<div class="form-group" data-group="url">
			<label for="connect_gal_name">Url</label>
			<input type="text" id="connect_gal_url" name="url" data-field="url" title="URL" class="form-control" placeholder="" value="{connect_gal.url}"><br />
		</div>
		<div class="form-group" data-group="text">
			<label for="connect_gal_text">Text</label>
			<textarea id="connect_gal_text" name="text" data-field="text" class="form-control" style="padding:10px;">{connect_gal.text}</textarea>
		</div>
		<input id="connect_gal_gid" name="whid" type="hidden" data-field="gid" value="{connect_gal.gid}">
	</form>
</div>
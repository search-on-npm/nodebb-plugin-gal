<div>
    <div>
    	<input id="group-search" type="text" class="form-control" placeholder="Search" /><br/>
		<table id="gal-list" class="table table-striped groups-list">
			<tr class="gal-notsearch">
				<th>Word</th>
				<th>Url</th>
				<th>Description</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
			<!-- BEGIN items -->
			<tr class="item" data-gid="{items.gid}">
				<td>{items.name}</td>
				<td><a href="{items.url}" target="_blank">{items.url}</a></td>
				<td>{items.text}</td>
				<td class="gal-notsearch">
					<button class="btn btn-default item_update" data-gid="{items.gid}"><i class="fa fa-edit"></i> Modifica</button>
				</td>
				<td class="gal-notsearch">
					<button class="btn btn-danger item_delete" data-gid="{items.gid}"><i class="fa fa-trash"></i> Cancella</button>
				</td>
			</tr>
			<!-- END items -->
		</table>
    </div>
    <button id="item_new" data-action="create" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
        <i class="material-icons">add</i>
    </button>
</div>
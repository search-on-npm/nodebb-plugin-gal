# GAL per Connect

**[FUNZIONALITA']**<br />

Questo plugin permette di specificare lato amministratore un nome, un link e una descrizione di un item. <br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-gal/raw/demaoui/screenshot/GAL-addNuovoItem.png)

<br />
Una volta inseriti nome,link,descrizione è possibile aggiungere il gal alla lista di gal sottostanti e salvare tramite l'apposito pulsante:<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-gal/raw/demaoui/screenshot/GAL-adminsave.png)<br/>


Quando un utente pubblicherà un post in cui è presente la parola specificata lato admin allora questa parola comparirà linkata in verde con una freccia prima di essa stessa, solo una volta per post e al passaggio del mouse si aprirà una sorta di alt contenente la descriione inserita dall'admin:

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-gal/raw/demaoui/screenshot/GAL-utenteitem.png)<br/>

E cliccando sulla parola linkata si aprirà uan pagina il cui link sarà il link specificato lato admin per quel GAL:

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-gal/raw/demaoui/screenshot/GAL-google.png)<br/>

<br />
**[TEST]**<br />
Per testare il pugin bisogna andare lato admin e inserire i valori di nome,link,descrizione per creare un GAL. Successivamente provare a scrivere un post, in una qualsiasi discussione, in cui si inserisce la parola specificata lato admin e vedere se questa parola si colorerà di verde e se, una volta premuto sul link, si aprirà una nuova pagina.









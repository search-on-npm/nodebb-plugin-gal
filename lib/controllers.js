'use strict';

var db = require.main.require('./src/database'),
	async = require.main.require('async'),
	winston = require.main.require('winston');

var Controllers = {};

Controllers.renderAdmin = function (req, res, next) {
	var start = 0;
	var stop = -1;

	var set = 'connect-gal';

	async.waterfall([
		function (next) {
			db.getSortedSetRange(set, start, stop, next);
		},
		function (gids, next) {
			if (!Array.isArray(gids) || !gids.length) {
				return next(null, []);
			}

			var keys = gids.map(function(gid) {
				return set+':gid:' + gid;
			});

			db.getObjects(keys, function(err, items) {
				if (err) {
					return next(err);
				}
				next(null, items);
			});
		}
	], function(err, items) {
		if(err) {
			return next(err);
		}
		res.render('admin/plugins/connect-gal', {items:items, next: stop + 1});
	});
};

module.exports = Controllers;
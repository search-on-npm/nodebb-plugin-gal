'use strict';

var db = require.main.require('./src/database'),
	winston = require.main.require('winston'),
	async = require.main.require('async'),
	meta = require.main.require('./src/meta');

var Sockets = {};

Sockets.save = function(socket, data, callback) {

	var set = 'connect-gal';

	if(!data || !data.data) {
		return callback({err:"data non presents"}, {message: 'Si è verificato un errore grave cod.65791'});
	}
	for(var i in data.data) {
		if(data.data[i]!=="" && !isNaN(data.data[i]) && i!=='name')
			data.data[i] = parseInt(data.data[i]);
	}
	if(data.data.gid==="") {
		winston.verbose("[connect-gal] Sockets.saveNew");
		async.waterfall([
			function(next) {
				winston.verbose("[connect-gal] get last gid");
				meta.settings.get('connect-gal', next);
			},
			function(lastgid, next) {
				// Imposto il nuovo gid (default: 1)
				var gid = 1;
				// Se sono presenti altri gid, il nuovo gid sarà uguale all'ultimo + 1
				if(lastgid && lastgid.counter) {
					winston.verbose("[connect-gal] last gid = " + lastgid.counter);
					gid = parseInt(lastgid.counter) + 1;
				}
				winston.verbose("[connect-gal] new gid = " + gid);

				winston.verbose("[connect-gal] add gid set");
				//Lo score è la lunghezza della parola, in questo modo sarà possibile ordinare dalla più lunga alla più breve
				//Potrà servire nel caso si voglia effettuare il match non solo della parola intera ma anche di una sua parte
				db.sortedSetAdd(set, data.data.name.length, gid, function(err) {
					winston.verbose("[connect-gal] gid set added");
					next(err, gid);
				});
			},
			// Salvo GAL
			function(gid, next) {
				//aggiungo il campo per l'id'
				data.data.gid = gid;
				winston.verbose("[connect-gal] add gid");
				db.setObject(set+':gid:' + gid, data.data, function(err) {
					next(err, gid);
				});
			},
			//incremento il counter
			function(gid, next) {
				meta.settings.set('connect-gal', {counter:gid}, next);
			}
		], function(err) {
			if(err) {
				winston.error("[connect-gal] add gid Error" + JSON.stringify(err));
				return callback(err, {message: 'Si è verificato un errore grave'});
			}
			winston.verbose("[connect-gal] Salvato gid");
			return callback(null, {title: 'GAL added', message: 'Restart to apply this change on oldest posts'});
		});
	} else {
		db.setObject(set+':gid:' + data.data.gid, data.data, function(err) {
			if(err) {
				winston.error("[connect-gal] add gid Error" + JSON.stringify(err));
				return callback(err, {message: 'Si è verificato un errore grave'});
			}
			winston.verbose("[connect-gal] Salvato gid");
			return callback(null, {title: 'GAL updated', message: 'Restart to apply this change on oldest posts'});
		});
	}
};

Sockets.get = function(socket, data, callback) {

	var set = 'connect-gal';

	if(!data || !data.gid) {
		return callback({err:"data non presents"}, {message: 'Si è verificato un errore grave cod.9578'});
	}

	db.getObject(set+':gid:' + data.gid, function(err, item) {
		if (err) {
			return callback(err, {message: 'Si è verificato un errore grave'});
		}
		return callback(null, item);
	});

};

Sockets.delete = function(socket, data, callback) {

	var set = 'connect-gal';

	if(!data || !data.gid) {
		return callback({err:"data non presents"}, {message: 'Si è verificato un errore grave cod.9578'});
	}

	async.waterfall([
		function(next) {
			db.sortedSetRemove(set, data.gid, next);
		},
		function(next) {
			db.delete(set + ":gid:" + data.gid, next);
		},
	], function(err) {
		if(err) {
			winston.error("[connect-gal] sort Error 2: " + JSON.stringify(err));
			return callback(err, {message: 'Si è verificato un errore grave cod.248464'});
		}
		winston.verbose("[connect-gal] Deleted gid: " + data.gid);
		return callback(null, {title: 'GAL deleted', message: 'Restart to apply this change on oldest posts'});
	});

};

module.exports = Sockets;